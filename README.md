# flexdc

![](https://img.shields.io/badge/written%20in-C%2B%2B%20and%20Flex%20%2F%20Actionscript-blue)

A web interface to an NMDC hub.

Using flash sockets eliminates dcwebui's need for a proxy server.

Tags: nmdc

This project has been superceded by [nmdc-webfrontend](https://code.ivysaur.me/nmdc-webfrontend/).

## See Also

Original thread: http://forums.apexdc.net/topic/4203-flexdc/


## Download

- [⬇️ flexdc-r0.1.rar](dist-archive/flexdc-r0.1.rar) *(546.64 KiB)*
- [⬇️ flexdc-1.1.rar](dist-archive/flexdc-1.1.rar) *(568.78 KiB)*
- [⬇️ flexdc-1.0-src.rar](dist-archive/flexdc-1.0-src.rar) *(6.08 KiB)*
- [⬇️ flexdc-1.0-release.rar](dist-archive/flexdc-1.0-release.rar) *(538.63 KiB)*
